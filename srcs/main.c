#include "hashtable.h"

size_t	hsh_hash(const char *str)
{
	int	hash;
	size_t	i;

	hash = 5381;
	i = 0;
	while (str[i++] != '\0')
	{
		hash = ((hash << 5) + hash) + str[i];
	}
	return (hash);
}

t_hashtable	*hsh_new(size_t size_max)
{
	t_hashtable	*hsh;
	
	if (!(hsh = (t_hashtable *)ft_memalloc(sizeof(t_hashtable *))))
		return (NULL);
	if (!(hsh->content = (t_hashcell **)ft_memalloc(sizeof(t_hashcell) * size_max)))
		return (NULL);
	hsh->size_max = size_max;
	return (hsh);
}

void	hsh_add(t_hashtable *hsh, size_t key, void *content, size_t content_size)
{
	t_hashcell	*cell;

	if (hsh->content[key] != NULL)
	{
		perror("Collision detected.");
		return ;
	}
	else
	{
		if (!(cell = (t_hashcell *)ft_memalloc(sizeof(t_hashcell))))
			return ;
		if (!(content) || !(content_size))
		{
			cell->content = NULL;
			cell->content_size = 0;
		}
		else
		{
			cell->content = ft_memalloc(sizeof(content_size));
			if (cell->content == NULL)
				return ;
			ft_memcpy(cell->content, content, content_size);
			cell->content_size = content_size;	
		}
		hsh->content[key] = cell;
	}
}

int	main(void)
{
	t_hashtable	*hsh;

	hsh = hsh_new(12);
	if (!hsh)
		perror("hsh null");
	hsh_add(hsh, (hsh_hash("Jerome Dody") % 12), ft_strdup("0622686456"), ft_strlen("0622686456"));
	ft_putendl((char *)(hsh->content[(hsh_hash("Jerome Dody") % 12)]->content));
	return (0);
}
