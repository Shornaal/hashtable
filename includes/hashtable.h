#ifndef FT_HASHTABLE_H
# define FT_HASHTABLE_H

#include <libft.h>

typedef struct		s_hashcell
{
	void		*key;
	void		*content;
	size_t		content_size;
}			t_hashcell;

typedef struct		s_hashtable
{
	t_hashcell	**content;
	size_t		size_max;
}			t_hashtable;

t_hashtable		*hsch_new(size_t size_max);
// void			*hsch_search(t_hahstable *hsht, void *key);
void			hsch_add(t_hashtable *hsht, void *key, void *content, size_t content_size);
// void			hsch_del(t_hashtable *hsht, void *key);
int			hsch_hash(const char *str);


#endif
