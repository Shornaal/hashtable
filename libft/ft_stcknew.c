/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stcknew.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 21:40:25 by tiboitel          #+#    #+#             */
/*   Updated: 2014/11/27 16:51:27 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

t_stack	*ft_stcknew(size_t size_max)
{
	t_stack		*newStack;

	if (!size_max)
		return (NULL);
	if ((newStack = (t_stack *)ft_memalloc(sizeof(t_stack))) == NULL)
		return (NULL);
	newStack->element = (t_stckElement **)ft_memalloc(sizeof(t_stckElement) * \
			size_max);
	if (newStack->element == NULL)
		return (NULL);
	newStack->top = -1;
	return (newStack);
}
