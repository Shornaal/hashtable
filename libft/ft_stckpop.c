/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stckpop.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/27 16:47:34 by tiboitel          #+#    #+#             */
/*   Updated: 2014/11/27 18:45:12 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

t_stckElement *ft_stckpop(t_stack **stack)
{
	t_stckElement *elem;

	if (!stack || !*stack)
		return (NULL);
	elem  = (*stack)->element[(*stack)->top];
	free((*stack)->element[(*stack)->top]);
	(*stack)->top--;
	if (elem == NULL)
		return (NULL);
	return (elem);
}
